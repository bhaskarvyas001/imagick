# README #

This Project demonstrates how can you use the ImageMagick plugin in PHP to convert images / PDF from one format to another. More details about imageMagick here --> [About ImageMagick](http://php.net/manual/en/intro.imagick.php)


### How do I get set up? ###
Follow these steps to properly install and enable ImageMagick in you environment.

1. Open PHPInfo and check:
   Architecture = x86 or x64
   Thread Safety = yes or no

2. Download ImageMagick from:
   [http://windows.php.net/downloads/pecl/deps/](http://windows.php.net/downloads/pecl/deps/)
   In my case I downloaded: ImageMagick-6.9.3-7-vc11-x86.zip
   because the Architecture under PHPInfo is x86
   as for vc11 or vc14
   search google for "visual c++ 11 runtime" or
   "visual c++ 14 runtime" and install it
 
3. Unzip and copy all dlls from the bin subfolder to the
	Apache bin directory. It's a bunch of CORE_RL_*.dll
	and IM_MOD_RL_*.dll plus a few other dlls.

	In my case, I installed XAMPP in C:\XAMPP, so:
	(from zip) bin/*.dll --> C:\XAMPP\apache\bin

4. Goto: [http://pecl.php.net/package/imagick](http://pecl.php.net/package/imagick)

    as of today, latest is 3.4.1 so I went to:
    [http://pecl.php.net/package/imagick/3.4.1/windows](http://pecl.php.net/package/imagick/3.4.1/windows)

    My PHP version is: 5.6.18, and Thread Safety is Yes from Step #1, so I downloaded:
    5.6 Thread Safe (TS) x86
    I got: php_imagick-3.4.1-5.6-ts-vc11-x86.zip

3. Unzip and copy "php_imagick.dll" to the php extension folder:
    In my case:
    php_imagick.dll --> C:\XAMPP\php\ext

4. Edit "php.ini" and add at the very end 
      [Imagick]
      extension=php_imagick.dll
   (Location of php.ini for me was C:\XAMPP\php\php.ini)

5. Restart Apache

6. Check PHPInfo
   scroll to section (or find): imagick    
   number of supported formats: 234

   If there is no "imagick" section or "supported format" is 0,
   something went wrong. Try all the steps carefully again
